package ru.korotkov.tm.repository;

import ru.korotkov.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    public Task create(final String name) {
        final Task task = new Task(name);
        tasks.add(task);
        return task;
    }

    public Task create(final String name, final String description) {
        final Task task = new Task(name, description);
        tasks.add(task);
        return task;
    }

    public void clear() {
        tasks.clear();
    }

    public int getSize() {
        return tasks.size();
    }

    public Task findByIndex(final int index) {
        return tasks.get(index);
    }

    public Task findByName(final String name) {
        for (final Task task : tasks) {
            if (name.equals(task.getName())) {
                return task;
            }
        }
        return null;
    }

    public Task findById(final Long id) {
        for (final Task task : tasks) {
            if (id.equals(task.getId())) {
                return task;
            }
        }
        return null;
    }

    public Task removeByIndex(final int index) {
        Task task = findByIndex(index);
        if (task != null) {
            tasks.remove(task);
        }
        return task;
    }

    public Task removeByName(final String name) {
        Task task = findByName(name);
        if (task != null) {
            tasks.remove(task);
        }
        return task;
    }

    public Task removeById(final Long id) {
        Task task = findById(id);
        if (task != null) {
            tasks.remove(task);
        }
        return task;
    }

    public Task updateByIndex(final int index, final String name) {
        Task task = findByIndex(index);
        if (task == null) {
            return null;
        }
        task.setName(name);
        return task;
    }

    public Task updateByIndex(final int index, final String name, final String description) {
        Task task = findByIndex(index);
        if (task == null) {
            return null;
        }
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    public Task updateById(final Long id, final String name) {
        Task task = findById(id);
        if (task == null) {
            return null;
        }
        task.setName(name);
        return task;
    }

    public Task updateById(final Long id, final String name, final String description) {
        Task task = findById(id);
        if (task == null) {
            return null;
        }
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    public List<Task> findAll() {
        return new ArrayList<>(tasks);
    }

}