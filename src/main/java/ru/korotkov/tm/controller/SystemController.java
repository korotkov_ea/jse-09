package ru.korotkov.tm.controller;

public class SystemController extends AbstractController {

    public void displayWelcome() {
        System.out.println(bundle.getString("welcome"));
    }

    public void displayVersion() {
        System.out.println(bundle.getString("version"));
    }

    public void displayAbout() {
        System.out.println(bundle.getString("about"));
    }

    public void displayHelp() {
        System.out.println(bundle.getString("help"));
    }

    public void displayStub(final String line) {
        System.out.println(String.format(bundle.getString("stub"), line));
    }

}